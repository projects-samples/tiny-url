
@extends('default')

@section('title')
TinyURL - Generer
@endsection

@section('create')
<div class="content">
	<h1>Raccourcir un nouveau lien</h1>
	<form class="form" method="post" action="">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="url" class="form-label">URL:</label>
			<input type="text" name="url" id="url" class="form-item" placeholder="http://mywebsite.com" required="required">
		</div>
		<button class="form-btn">Raccourcir</button>
	</form>
</div>
@endsection

