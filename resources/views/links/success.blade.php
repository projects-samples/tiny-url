@extends('default')

@section('title')
TinyURL - Success
@endsection

@section('success')

	<div class="success">
		<h1>Merci d'avoir utilise notre service</h1>
		<p> L'url se trouve a l'adresse 
			<a class="success-link" href="{{ action('LinksController@show', ['id' => $link->id]) }}" class="btn-success">
				{{ action('LinksController@show', ['id' => $link->id]) }}
			</a>
		</p>
	</div>
@endsection