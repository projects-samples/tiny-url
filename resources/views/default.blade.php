
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<style>
		body {
			margin: 0;
			padding: 0;
			font-family: "Raleway", sans-serif;
			background-color: #EEEEEF;
			color: #242424;
		}
		h1 {
			color: #0D5D50;
		}

		a {
			text-decoration: none;
			color: inherit;
			font-size: 20px;
			display: inline-block;
			margin-left: 7px;
		}

		.container {
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.content {
			width: 100%;
			display: flex;
			flex-flow: column nowrap;
			align-items: center;
		}
		.form {
			margin-top: 70px;
			width: 40%;
			display: flex;
			font-size: 20px;
		}

		.form-group {
			width: 100%;
			display: flex;
			align-items: center;
		}
		.form-item {
			flex-grow: 1;
			height: 38px;
			font-size: 20px;
			margin: 0 10px;
		}

		.form-btn {
			font-size: 20px;
			background-color: #3CA0DF;
			color: #434853;
			border-radius: 0 10px;
			transition: all .5s;
			cursor: pointer;
		}

		.form-btn:hover {
			background-color: #F9A20C;
			color: #000;
		}

		.success-link {
			transition: all .5s;
		}

		.success-link:hover {
			font-size: 25px;
		}

		div.success {
			margin-top: 200px;
			width: 100%;
			display: flex;
			flex-flow: row wrap;
			justify-content: center;
			align-items: center;
		}
		div.success>* {
			width: 100%;
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="container">
		
		@yield('create')
		
		@yield('success')
	</div>
</body>
</html>