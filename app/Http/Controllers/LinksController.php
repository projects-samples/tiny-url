<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Input;
use App\Link;

class LinksController extends Controller
{
    //
    public function show($id) {
    	$link = Link::findOrFail($id);
    	return redirect($link->url, 301);
    }
    public function create() {

    	return view('links.create');
    }

    public function store() {
    	// get the input url
    	$url = Input::get('url');
    	// check if its not already in db
    	$link = Link::firstOrCreate(['url' => $url]);
    	
    	return view('links.success', compact('link'));
    }
}
